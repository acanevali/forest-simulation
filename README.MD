# Simulation de feu de forêt
## Alexis Canevali
### Outils utilisés
- Projet Intellij IDEA
- Java SDK 17
- Librairie Jackson FasterXML

### Utilisation
Sur un shell, se placer dans le dossier de l'application puis entrer :
```shell
java -jar /exec/back-end.jar
```
Sur Postman ou sur un navigateur internet accéder à :
- Pour créer une nouvelle simulation sur 3 lignes et 5 colonnes: http://127.0.0.1:8080/newSimulation?nbLines=3&nbColumns=5
- Pour visualiser sur le shell l'évolution de la simulation:
http://127.0.0.1:8080/nextStepSimulation
### Architecture
- MVC
- Prévu pour être connecté à un front-end
- Affichage dans la console


