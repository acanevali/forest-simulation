package com.acanevali.model;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class ForestSimulation {

    private static ForestSimulation simulationInstance = null;
    private Forest simulatedForest;
    private ObjectMapper simulationMapper;
    private boolean isSimulationOver;
    private int nbSimulationTurns;

    private ForestSimulation(int nb_lines,int nb_columns) {
        this.simulatedForest = new Forest(nb_lines,nb_columns);
        this.simulationMapper = new ObjectMapper();
        this.isSimulationOver = false;
        this.nbSimulationTurns = 0;
    }

    public Forest getSimulatedForest() {
        return simulatedForest;
    }

    private void checkIfSimulationOver(){
        this.isSimulationOver = (this.simulatedForest.getForestUnitsOnFire().size()==0);
    }

    public boolean isSimulationOver(){
        return this.isSimulationOver;
    }

    public void startSimulation(){
        FirePropagationStrategy.startSimulation(this.simulatedForest);
    }

    public void nextStepSimulation(){
        if(!this.isSimulationOver()){
            FirePropagationStrategy.nextStepSimulation(this.simulatedForest);
            this.nbSimulationTurns++;
        }
        this.checkIfSimulationOver();
    }

    public String getSimulationAsJSON() throws JsonProcessingException {
        ObjectWriter currentSimulationWriter = this.simulationMapper.writer();
        return currentSimulationWriter.writeValueAsString(this.simulatedForest);
    }

    public void printSimulationStats(){
        this.checkIfSimulationOver();
        if(this.isSimulationOver()){
            System.out.println("Simulation terminée.");
        } else {
            System.out.println("Simulation en cours.");
        }

        System.out.println("Nb arbres brûlés : "+this.simulatedForest.getForestUnitsBurned().size());
    }

    public static ForestSimulation getForestSimulationInstance(int nb_lines,int nb_columns){
        //if(simulationInstance == null){
            simulationInstance = new ForestSimulation(nb_lines,nb_columns);
        //}
        return simulationInstance;
    }

    public static ForestSimulation getForestSimulationInstance(){
        return simulationInstance;
    }

}
