package com.acanevali.model;

public enum UnitState {
    Vivant,
    Incendie,
    Calcine
}
