package com.acanevali.model;

import java.util.ArrayList;

public class FirePropagationStrategy {
    static int randomIgnitionValue = 90;
    static int randomPropagationValue = 75;

    public static void randomFireIgnition(ForestUnit forestUnit){
        int ignition_int = (int) Math.floor(Math.random() * 100);
        if (ignition_int >= randomIgnitionValue) {
            forestUnit.unitState = UnitState.Incendie;
        }
    }

    public static void randomFirePropagation(ForestUnit forestUnit){
        int propagation_int = (int) Math.floor(Math.random() * 100);
        if ((propagation_int >= randomPropagationValue)&&(forestUnit.getUnitState()==UnitState.Vivant)) {
            forestUnit.unitState = UnitState.Incendie;
        }
    }

    public static void nearestNeighbourRandomFirePropagation(Forest simulatedForest, ArrayList<ForestUnit> unitsBurned){
        for(ForestUnit currentBurnedUnit: unitsBurned){
            ArrayList<ForestUnit> neighbourUnits = new ArrayList<ForestUnit>();
            neighbourUnits.add(simulatedForest.getForestUnits().get(new UnitCoordinate(currentBurnedUnit.getUnitCoordinate().getLine_index() - 1,currentBurnedUnit.getUnitCoordinate().getColumn_index())));
            neighbourUnits.add(simulatedForest.getForestUnits().get(new UnitCoordinate(currentBurnedUnit.getUnitCoordinate().getLine_index() + 1,currentBurnedUnit.getUnitCoordinate().getColumn_index())));
            neighbourUnits.add(simulatedForest.getForestUnits().get(new UnitCoordinate(currentBurnedUnit.getUnitCoordinate().getLine_index(),currentBurnedUnit.getUnitCoordinate().getColumn_index() - 1)));
            neighbourUnits.add(simulatedForest.getForestUnits().get(new UnitCoordinate(currentBurnedUnit.getUnitCoordinate().getLine_index(),currentBurnedUnit.getUnitCoordinate().getColumn_index() + 1)));
            for(ForestUnit currentNeigbourUnit: neighbourUnits){
                if(currentNeigbourUnit != null){
                    FirePropagationStrategy.randomFirePropagation(currentNeigbourUnit);
                }
            }
        }
    }

    public static void startSimulation(Forest simulatedForest){
        UnitCoordinate currentUnitCoordinate;
        ForestUnit forestUnit;
        for(int i=0; i < simulatedForest.getNb_lines(); i++){
            for(int j=0; j < simulatedForest.getNb_columns(); j++){
                currentUnitCoordinate = new UnitCoordinate(i,j);
                forestUnit = simulatedForest.getForestUnits().get(currentUnitCoordinate);
                if(forestUnit != null){
                    FirePropagationStrategy.randomFireIgnition(forestUnit);
                } else {
                    System.out.println("forestUnit"+i+":"+j+" Null");
                }
            }
        }
    }

    public static void nextStepSimulation(Forest simulatedForest){
        ArrayList<ForestUnit> unitsOnFire = simulatedForest.getForestUnitsOnFire();

        for(ForestUnit currentUnitOnFire: unitsOnFire){
            currentUnitOnFire.unitState = UnitState.Calcine;
        }

        FirePropagationStrategy.nearestNeighbourRandomFirePropagation(simulatedForest,unitsOnFire);
    }

}
