package com.acanevali.model;

import java.util.ArrayList;
import java.util.HashMap;

public class Forest {
    private int nb_lines;
    private int nb_columns;
    private HashMap<UnitCoordinate,ForestUnit> forestUnits;

    public Forest(int nb_lines, int nb_columns) {
        this.nb_lines = nb_lines;
        this.nb_columns = nb_columns;
        forestUnits = new HashMap<UnitCoordinate,ForestUnit>();
        UnitCoordinate currentUnitCoordinate;
        ForestUnit currentForestUnit;

        for(int i=0; i<nb_lines; i++){
            for(int j=0; j<nb_columns; j++){
                currentUnitCoordinate = new UnitCoordinate(i,j);
                currentForestUnit = new ForestUnit(currentUnitCoordinate);
                forestUnits.put(currentUnitCoordinate,currentForestUnit);
            }
        }
    }

    public int getNb_lines() {
        return nb_lines;
    }

    public int getNb_columns() {
        return nb_columns;
    }

    public HashMap<UnitCoordinate, ForestUnit> getForestUnits() {
        return forestUnits;
    }

    public ArrayList<ForestUnit> getForestUnitsOnFire(){
        UnitCoordinate currentUnitCoordinate;
        ForestUnit forestUnit;
        ArrayList<ForestUnit> unitsOnFire = new ArrayList<>();
        for(int i=0; i < this.getNb_lines(); i++){
            for(int j=0; j < this.getNb_columns(); j++){
                currentUnitCoordinate = new UnitCoordinate(i,j);
                forestUnit = this.getForestUnits().get(currentUnitCoordinate);
                if(forestUnit != null){
                    if(forestUnit.unitState==UnitState.Incendie){
                        unitsOnFire.add(forestUnit);
                    }
                } else {
                    System.out.println("forestUnit"+i+":"+j+" Null");
                }
            }
        }
        return unitsOnFire;
    }

    public ArrayList<ForestUnit> getForestUnitsBurned(){
        UnitCoordinate currentUnitCoordinate;
        ForestUnit forestUnit;
        ArrayList<ForestUnit> unitsBurned = new ArrayList<>();
        for(int i=0; i < this.getNb_lines(); i++){
            for(int j=0; j < this.getNb_columns(); j++){
                currentUnitCoordinate = new UnitCoordinate(i,j);
                forestUnit = this.getForestUnits().get(currentUnitCoordinate);
                if(forestUnit != null){
                    if(forestUnit.unitState==UnitState.Calcine){
                        unitsBurned.add(forestUnit);
                    }
                } else {
                    System.out.println("forestUnit"+i+":"+j+" Null");
                }
            }
        }
        return unitsBurned;
    }

    public void printForest(){
        UnitCoordinate currentUnitCoordinate;
        ForestUnit forestUnit;
        for(int i=0; i < this.getNb_lines(); i++){
            System.out.print("|");
            for(int j=0; j < this.getNb_columns(); j++){
                currentUnitCoordinate = new UnitCoordinate(i,j);
                forestUnit = this.getForestUnits().get(currentUnitCoordinate);
                if(forestUnit != null){
                    switch (forestUnit.getUnitState()){
                        case Vivant -> System.out.print(" ");
                        case Incendie -> System.out.print("X");
                        case Calcine -> System.out.print("O");
                        default -> throw new IllegalStateException("Unexpected value: " + forestUnit.getUnitState());
                    }
                } else {
                    System.err.println("forestUnit "+i+":"+j+" Null");
                }
                System.out.print("|");
            }
            System.out.println();
        }
        System.out.println();
    }

}
