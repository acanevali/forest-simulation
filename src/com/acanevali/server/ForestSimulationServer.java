package com.acanevali.server;

import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import com.acanevali.controller.*;

public class ForestSimulationServer {
    public static void launchServer() throws IOException {
        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);
        HttpServer server = HttpServer.create(new InetSocketAddress("localhost",8080),0);
        server.createContext("/newSimulation", new NewSimulationController());
        server.createContext("/nextStepSimulation", new NextStepSimulationController());
        server.setExecutor(threadPoolExecutor);
        server.start();
        System.out.println("Serveur démarré sur http://localhost:8080");
    }
}
