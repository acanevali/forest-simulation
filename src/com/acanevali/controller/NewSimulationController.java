package com.acanevali.controller;

import com.acanevali.model.ForestSimulation;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.OutputStream;

public class NewSimulationController extends GeneralController {

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {
            super.handle(exchange);
            String requestParamValue = exchange.getRequestURI().toString();
            this.handleResponse(exchange);
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
    }

    private void handleResponse(HttpExchange httpExchange)  throws  IOException {
        String uriAndParams[] = httpExchange.getRequestURI().toString().split("\\?");
        String uri = null;
        if(uriAndParams.length==2){
            uri = uriAndParams[1];
        }
        int nb_lines = 10;
        int nb_columns = 5;

        if(uri!=null){
            String params[] = uri.split("&");
            String param_name;
            String str_param_value;
            int param_value;
            for(String currentParam: params){
                param_name = currentParam.split("=")[0];
                str_param_value = currentParam.split("=")[1];
                try{
                    param_value = Integer.parseInt(str_param_value);
                    switch (param_name) {
                        case "nbLines":
                            nb_lines = param_value;
                            break;
                        case "nbColumns":
                            nb_columns = param_value;
                            break;
                        default:
                            break;
                    }
                } catch (NumberFormatException e){
                    System.err.println("Mauvais paramètre(s) de configuration fourni(s) dans l'URL");
                }
            }
        }

        ForestSimulation simulation = ForestSimulation.getForestSimulationInstance(nb_lines,nb_columns);
        simulation.startSimulation();
        simulation.getSimulatedForest().printForest();
        String forestJSON = simulation.getSimulationAsJSON();

        OutputStream outputStream = httpExchange.getResponseBody();
        httpExchange.getResponseHeaders().add("Content-Type","application/json");
        httpExchange.sendResponseHeaders(200, forestJSON.length());
        outputStream.write(forestJSON.getBytes());
        outputStream.flush();
        outputStream.close();

    }


}
