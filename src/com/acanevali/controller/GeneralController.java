package com.acanevali.controller;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;

public class GeneralController implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String requestParamValue=null;

        if(!"GET".equals(exchange.getRequestMethod())) {
            this.handleUnknownRequest(exchange);
        }
    }

    private void handleUnknownRequest(HttpExchange httpExchange)  throws  IOException {

        OutputStream outputStream = httpExchange.getResponseBody();
        String message = "{ \"Error\" : \"Unknown query\" }";
        httpExchange.sendResponseHeaders(400, message.length());
        outputStream.write(message.getBytes());
        outputStream.flush();
        outputStream.close();

    }


}